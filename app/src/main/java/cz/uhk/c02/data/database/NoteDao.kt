package cz.uhk.c02.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(note: NoteEntity)

    @Query("SELECT * FROM NoteEntity")
    suspend fun selectAll(): List<NoteEntity>

    @Query("SELECT * FROM NoteEntity WHERE id=:id")
    suspend fun selectAll(id: Long): NoteEntity?

    @Query("SELECT * FROM NoteEntity")
    fun selectAllFlow(): Flow<List<NoteEntity>>

}