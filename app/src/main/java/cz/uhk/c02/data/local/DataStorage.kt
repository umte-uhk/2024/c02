package cz.uhk.c02.data.local

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DataStorage(
    val dataStore: DataStore<Preferences>
) {

    val textFlow: Flow<String?> = dataStore.data.map { it[TextKey] }

    suspend fun setTextData(value: String) {
        dataStore.edit {
            it[TextKey] = value
        }
    }

    val TextKey = stringPreferencesKey("TextData")

}