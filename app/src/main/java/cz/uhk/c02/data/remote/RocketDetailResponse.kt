package cz.uhk.c02.data.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RocketDetailResponse(
    @SerialName("rocket_name") val name: String,
    val description: String,
    val wikipedia: String?,
)