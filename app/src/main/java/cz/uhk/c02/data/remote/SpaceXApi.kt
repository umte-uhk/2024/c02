package cz.uhk.c02.data.remote

import cz.uhk.c02.data.LaunchResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface SpaceXApi {

    @GET("launches")
    suspend fun fetchLaunches(): List<LaunchResponse>

    @GET("rockets/{rocketId}")
    suspend fun fetchRocketDetail(
        @Path("rocketId") rocketID: String,
    ): RocketDetailResponse?

}