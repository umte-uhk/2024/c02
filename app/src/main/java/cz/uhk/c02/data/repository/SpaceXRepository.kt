package cz.uhk.c02.data.repository

import cz.uhk.c02.data.remote.SpaceXApi

class SpaceXRepository(
    private val api: SpaceXApi,
) {
    suspend fun fetchAllSuccessfulLaunches() =
        api.fetchLaunches()

    suspend fun fetchRocketDetail(id: String) =
        api.fetchRocketDetail(id)
}