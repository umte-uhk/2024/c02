package cz.uhk.c02.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.uhk.c02.data.database.AppDatabase
import cz.uhk.c02.data.remote.SpaceXApi
import cz.uhk.c02.data.local.DataStorage
import cz.uhk.c02.data.repository.SpaceXRepository
import cz.uhk.c02.ui.database.DatabaseViewModel
import cz.uhk.c02.ui.datastore.DataStoreViewModel
import cz.uhk.c02.ui.launches.detail.RocketDetailViewModel
import cz.uhk.c02.ui.launches.list.RocketLaunchesViewModel
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val appModules by lazy {
    listOf(dataModule, uiModule)
}

val dataModule = module {
    single { createRetrofit() }
    single { get<Retrofit>().create(SpaceXApi::class.java) }
    single { DataStorage(androidApplication().dataStore) }
    single { SpaceXRepository(get()) }

    single<AppDatabase> {
        Room.databaseBuilder(
            context = androidApplication(),
            klass = AppDatabase::class.java,
            name = "umte_database"
        ).build()
    }
    single { get<AppDatabase>().getNoteDao() }
}

val uiModule = module {
    viewModel { RocketLaunchesViewModel(get()) }
    viewModel { DataStoreViewModel(get()) }
    viewModel { DatabaseViewModel(get()) }
    viewModel { (rocketId: String) -> RocketDetailViewModel(rocketId, get()) }
}

private fun createRetrofit(
    client: OkHttpClient = createOkHttpClient(),
    baseUrl: String = BaseApiUrl
) = Retrofit.Builder()
    .client(client)
    .baseUrl(baseUrl)
    .addConverterFactory(
        Json {
            ignoreUnknownKeys = true
        }.asConverterFactory(MediaType.get("application/json"))
    ).build()

private fun createOkHttpClient() = OkHttpClient.Builder().build()

private const val BaseApiUrl = "https://api.spacexdata.com/v3/" // launches

private const val DataStoreName = "UMTEStore"
private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(DataStoreName)