package cz.uhk.c02.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cz.uhk.c02.ui.database.DatabaseScreen
import cz.uhk.c02.ui.datastore.DataStoreScreen
import cz.uhk.c02.ui.home.HomeScreen
import cz.uhk.c02.ui.launches.detail.RocketDetailScreen
import cz.uhk.c02.ui.launches.list.RocketLaunchesScreen
import cz.uhk.c02.ui.notification.NotificationScreen

@Composable
fun AppContainer() {
    val controller = rememberNavController()

    NavHost(navController = controller, startDestination = DestinationHome) {
        composable(DestinationHome) {
            HomeScreen(
                controller
            )
        }
        composable(DestinationLaunches) {
            RocketLaunchesScreen(
                onNavigateDetail = { rocketId ->
                    controller.navigate(DestinationRocketDetail.replace("{rocketId}", rocketId))
                }
            )
        }
        composable(
            route = DestinationRocketDetail,
            arguments = listOf(navArgument("rocketId") { type = NavType.StringType }),
        ) { entry ->
            RocketDetailScreen(
                rocketId = entry.arguments?.getString("rocketId").orEmpty(),
            )
        }
        composable(DestinationDataStore) {
            DataStoreScreen()
        }
        composable(DestinationDatabase) {
            DatabaseScreen()
        }
        composable(DestinationNotification) {
            NotificationScreen()
        }
    }
}

const val DestinationHome = "home"
const val DestinationNotification = "notification"
const val DestinationLaunches = "launches"
private const val DestinationRocketDetail = "rocket/{rocketId}"
const val DestinationDataStore = "datastore"
const val DestinationDatabase = "database"

