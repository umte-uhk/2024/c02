package cz.uhk.c02.ui.basics.dialog

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember

@Composable
fun AlertScreen() {
    val dialogShown = remember {
        mutableStateOf(false)
    }

    Button(onClick = {
        dialogShown.value = true
    }) {
        Text(
            text = "Zobrazit dialog",
        )
    }

    if (dialogShown.value) {
        AlertDialog(
            onDismissRequest = {
                dialogShown.value = false
            },
            title = {
                Text(text = "Titulek")
            },
            text = {
                Column {
                    Text(text = "Obsah dialogu")
                    Text(text = "Druhy radek")
                }
            },
            confirmButton = {
                TextButton(onClick = {
                    dialogShown.value = false
                }) {
                    Text(text = "Zavrit")
                }
            },
        )
    }
}
