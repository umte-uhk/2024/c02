package cz.uhk.c02.ui.basics.dialog

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import cz.uhk.c02.ui.theme.UMTEAppTheme

class MyDialogActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            UMTEAppTheme {
                AlertScreen()
            }
        }
    }
}
