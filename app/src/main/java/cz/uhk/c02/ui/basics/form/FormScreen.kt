package cz.uhk.c02.ui.basics.form

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import cz.uhk.c02.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FormScreen() {
    val inputValue = remember {
        mutableStateOf("Test")
    }

    var inputValue2 by remember {
        mutableStateOf("Test")
    }

//    inputValue.value = "test"
//    inputValue2 = "test"

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Titulek")
                },
                modifier = Modifier.padding(4.dp),

            )
        },
        content = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxSize(),
            ) {
                Spacer(modifier = Modifier.height(100.dp))

                Row {
                    Image(
                        painter = painterResource(id = R.drawable.test),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier.width(300.dp),
                    )

                    Icon(painter = painterResource(id = R.drawable.ic_launcher_foreground), contentDescription = "")
                }

                Text("asdf")

                OutlinedTextField(
                    value = inputValue.value,
                    label = {
                        Text(text = "Popisek")
                    },
                    onValueChange = {
                        inputValue.value = it
                    },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Done,
                    ),
                )

                // TODO Radio button
            }

            it
        },
    )
}
