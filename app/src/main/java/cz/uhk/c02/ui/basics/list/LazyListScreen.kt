package cz.uhk.c02.ui.basics.list

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountBox
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import cz.uhk.c02.ui.theme.UMTEAppTheme

@Composable
fun LazyListScreen() {
    UMTEAppTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background,
        ) {
            val people = remember {
                generateRandomHumans(100)
            }
            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(8.dp),
                contentPadding = PaddingValues(8.dp),
            ) {
                items(people) { human ->
                    Card {
                        Row(
                            modifier = Modifier.padding(16.dp),
                        ) {
                            Text(text = human.name,
                                modifier = Modifier.weight(1f))
                            Icon(imageVector = human.status.icon(), contentDescription = "")
                        }
                    }
                }
            }
        }
    }
}

private val names = arrayOf("John", "Tomáš", "Petr", "Filip", "Alex")
private val surnames = arrayOf("Cena", "Kozel", "Weissar", "Oborník", "Jones")

data class Human(
    val name: String,
    val status: HumanStatus,
)

enum class HumanStatus {
    Okey, Favorite, Nice;

    fun icon(): ImageVector = when (this) {
        Okey -> Icons.Default.ThumbUp
        Favorite -> Icons.Default.Star
        Nice -> Icons.Default.AccountBox
    }
}

fun generateRandomHumansOld(count: Int): List<Human> {
    val list = mutableListOf<Human>()

    // for( int i = 0; i < count; i++)

    for (i in 0..count) {
        list.add(Human(name = names.random(), status = HumanStatus.values().random()))
    }

    return list
}

fun generateRandomHumans(count: Int) = mutableListOf<Human>().apply {
    repeat(count) {
        add(Human(name = "${names.random()} ${surnames.random()}", status = HumanStatus.values().random()))
    }
}
