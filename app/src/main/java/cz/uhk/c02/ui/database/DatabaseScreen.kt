package cz.uhk.c02.ui.database

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cz.uhk.c02.data.database.NoteEntity
import org.koin.androidx.compose.getViewModel

@Composable
fun DatabaseScreen(
    viewModel: DatabaseViewModel = getViewModel()
) {
    val inputValue = remember {
        mutableStateOf("")
    }
    
    val notes = viewModel.notes.collectAsState(initial = emptyList())

    Column {
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(16.dp),
            contentPadding = PaddingValues(all = 16.dp),
            modifier = Modifier
                .fillMaxWidth()
                .weight(1F),
            content = {
                items(notes.value) {note ->
                    Text(text = note.text)
                }
            }
        )
        
        Row {
            OutlinedTextField(value = inputValue.value, onValueChange = {
                inputValue.value = it
            })
            Button(onClick = {
                viewModel.insertNote(inputValue.value)
            }) {
                Text(text = "Pridat")
            }
        }
    }
}