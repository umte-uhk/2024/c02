package cz.uhk.c02.ui.database

import cz.uhk.c02.data.database.NoteDao
import cz.uhk.c02.data.database.NoteEntity
import cz.uhk.c02.ui.base.BaseViewModel

class DatabaseViewModel(
    private val noteDao: NoteDao
) : BaseViewModel() {

    val notes get() = noteDao.selectAllFlow()

    fun insertNote(text: String) = launch {
        noteDao.insertOrUpdate(
            NoteEntity(
                text = text,
            )
        )
    }
}