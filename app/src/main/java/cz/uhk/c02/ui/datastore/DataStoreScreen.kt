package cz.uhk.c02.ui.datastore

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.text.input.ImeAction
import kotlinx.coroutines.flow.last
import org.koin.androidx.compose.getViewModel

@Composable
fun DataStoreScreen(
    viewModel: DataStoreViewModel = getViewModel()
) {

    val inputValue = remember {
        mutableStateOf("")
    }

    val saveText = viewModel.textFlow.collectAsState(initial = "")

    Column {
        OutlinedTextField(
            value = inputValue.value,
            label = {
                Text(text = "Popisek")
            },
            onValueChange = {
                inputValue.value = it
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
            ),
        )

        Text("Ulozeny text")
        Text(saveText.value ?: "-") // vypisovat text

        Button(onClick = {
            viewModel.saveText(inputValue.value)
        }) {
            Text(text = "Ulozit")
        }
    }



}