package cz.uhk.c02.ui.datastore

import androidx.datastore.core.DataStore
import cz.uhk.c02.data.local.DataStorage
import cz.uhk.c02.ui.base.BaseViewModel

class DataStoreViewModel(
    private val dataStorage: DataStorage
): BaseViewModel() {

    val textFlow get() = dataStorage.textFlow

    fun saveText(value: String) = launch {
        dataStorage.setTextData(value)
    }

}