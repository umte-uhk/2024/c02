package cz.uhk.c02.ui.home

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import cz.uhk.c02.App
import cz.uhk.c02.ui.DestinationDataStore
import cz.uhk.c02.ui.DestinationDatabase
import cz.uhk.c02.ui.DestinationLaunches
import cz.uhk.c02.ui.DestinationNotification
import cz.uhk.c02.ui.basics.dialog.MyDialogActivity
import cz.uhk.c02.ui.basics.form.FormActivity
import cz.uhk.c02.ui.basics.list.LazyListActivity
import cz.uhk.c02.ui.datastore.DataStoreViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun HomeScreen(
    controller: NavHostController,
) {
    val context = LocalContext.current

    // A surface container using the 'background' color from the theme
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        // todo Home screen a přesunout do aktivity

        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            Text(
                text = "Vyber moznosti",
            )

            Button(onClick = {
                context.startActivity(Intent(context, FormActivity::class.java))
            }) {
                Column {
                    Text("Formular")
                }
            }

            Button(onClick = {
                context.startActivity(Intent(context, LazyListActivity::class.java))
            }) {
                Column {
                    Text("Lazy list")
                }
            }

            Button(onClick = {
                context.startActivity(Intent(context, MyDialogActivity::class.java))
            }) {
                Column {
                    Text("Dialog")
                }
            }

            Button(onClick = {
                controller.navigate(DestinationLaunches)
            }) {
                Column {
                    Text("Launches")
                }
            }

            Button(onClick = {
                controller.navigate(DestinationDataStore)
            }) {
                Column {
                    Text("DataStore screen")
                }
            }

            Button(onClick = {
                controller.navigate(DestinationDatabase)
            }) {
                Column {
                    Text("Database screen")
                }
            }

            Button(onClick = {
                controller.navigate(DestinationNotification)
            }) {
                Column {
                    Text("Open notification screen")
                }
            }
        }
    }
}
