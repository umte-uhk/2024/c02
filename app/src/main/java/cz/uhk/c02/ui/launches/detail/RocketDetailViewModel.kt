package cz.uhk.c02.ui.launches.detail

import cz.uhk.c02.data.remote.RocketDetailResponse
import cz.uhk.c02.data.repository.SpaceXRepository
import cz.uhk.c02.ui.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class RocketDetailViewModel(
    private val rocketId: String,
    private val repo: SpaceXRepository,
) : BaseViewModel() {

    private val _detail = MutableStateFlow<RocketDetailResponse?>(null)
    val detail = _detail.asStateFlow()

    init {
        launch {
            repo.fetchRocketDetail(rocketId)
                .also {
                    _detail.emit(it)
                }
        }
    }
}