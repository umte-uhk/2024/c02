package cz.uhk.c02.ui.launches.list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cz.uhk.c02.ui.base.State
import cz.uhk.c02.ui.theme.UMTEAppTheme
import org.koin.androidx.compose.getViewModel

@Composable
fun RocketLaunchesScreen(
    viewModel: RocketLaunchesViewModel = getViewModel(),
    onNavigateDetail: (String) -> Unit,
    ) {

    val state = viewModel.state.collectAsState()
    val list = viewModel.successfulLaunches.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        when (val stateValue = state.value) {
            State.None, State.Loading -> {
                CircularProgressIndicator()
            }

            is State.Failure -> {
                Button(onClick = { stateValue.repeat() }) {
                    Text(text = "Zkusit znovu")
                }
            }

            is State.Success -> {
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp),
                    contentPadding = PaddingValues(8.dp),
                ) {
                    items(list.value) { launch ->
                        Card {
                            Row(
                                modifier = Modifier
                                    .clickable { onNavigateDetail(launch.rocket.id) }
                                    .padding(16.dp)
                            ) {
                                Text(
                                    modifier = Modifier.fillMaxWidth(),
                                    text = launch.missionName + ": " + launch.flightNumber,
                                    style = MaterialTheme.typography.headlineMedium,
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}


@Preview
@Composable
private fun Preview() {
    UMTEAppTheme {
        RocketLaunchesScreen(onNavigateDetail = {})
    }
}