package cz.uhk.c02.ui.launches.list

import cz.uhk.c02.data.LaunchResponse
import cz.uhk.c02.data.repository.SpaceXRepository
import cz.uhk.c02.ui.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlin.random.Random

class RocketLaunchesViewModel(
    private val launchesRepo: SpaceXRepository,
) : BaseViewModel() {

    private val _successfulLaunches = MutableStateFlow<List<LaunchResponse>>(emptyList())
    val successfulLaunches = _successfulLaunches.asStateFlow()

    init {
        launch {
            // delay(1000L)
            val launches = launchesRepo.fetchAllSuccessfulLaunches()
            if (Random.nextBoolean()) throw Exception("Test")
            _successfulLaunches.emit(launches)
        }
    }
}