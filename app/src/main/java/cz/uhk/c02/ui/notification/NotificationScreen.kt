package cz.uhk.c02.ui.notification

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import cz.uhk.c02.App
import cz.uhk.c02.R
import java.util.Random

@Composable
fun NotificationScreen() {

    val context = LocalContext.current

    val permissionGranted = remember {
        mutableStateOf(context.isNotificationPermissionGranted())
    }

    val permissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted ->
            if (isGranted) {
                Toast.makeText(context, "Notifikace povoleny", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Notifikace NEpovoleny", Toast.LENGTH_SHORT).show()
            }

            permissionGranted.value = isGranted
        },
    )

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(
            onClick = {
                requestPermission(context, permissionLauncher)
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//                    checkAndRequestCameraPermission(context, Manifest.permission.POST_NOTIFICATIONS, launcher)
//                }
            }
        ) {
            Text("Ask fro permission")
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text("Permission status: ${permissionGranted.value}")

        Spacer(modifier = Modifier.height(64.dp))
        Button(
            onClick = {
                sendNotification(context)
            }
        ) {
            Text("Send push notification")
        }
    }

}

@SuppressLint("MissingPermission")
private fun sendNotification(context: Context) {
    val builder = NotificationCompat.Builder(context, App.MarketingNotificationChannelID)
        .setSmallIcon(R.drawable.ic_launcher_foreground)
        .setContentTitle("Titulek")
        .setContentText("Text notifikace muze byt delsi")
        .setPriority(NotificationCompat.PRIORITY_HIGH)

    if (context.isNotificationPermissionGranted().not()) {
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
        return
    } else {
        NotificationManagerCompat.from(context).notify(kotlin.random.Random.nextInt(), builder.build())
    }
}

private fun requestPermission(
    context: Context,
    launcher: ManagedActivityResultLauncher<String, Boolean>,
) {
    launcher.launch(Manifest.permission.POST_NOTIFICATIONS)
}

private fun Context.isNotificationPermissionGranted() =
    checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED